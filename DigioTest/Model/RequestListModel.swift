//
//  SpotlightListModel.swift
//  DigioTest
//
//  Created by Ian on 26/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import UIKit

public struct RequestListModel: Codable {
    
    public var spotlight: [SpotlightsModel]?
    public var cash: DigioCashModel?
    public var products: [ProductsModel]?
    
    public init(spotlight: [SpotlightsModel]? = nil, cash: DigioCashModel? = nil, products: [ProductsModel]? = nil) {
        
        self.spotlight = spotlight
        self.cash = cash
        self.products = products
    }
}
