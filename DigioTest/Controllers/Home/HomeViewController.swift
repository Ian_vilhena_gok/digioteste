//
//  ViewController.swift
//  DigioTest
//
//  Created by Ian on 26/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController {
    
    
    //MARK: - Properties
    var productsModel = [ProductsModel]()
    var spotlightsModel = [SpotlightsModel]()
    var digioCashModel = DigioCashModel()
    
    
    // SetupUI
    fileprivate let productsCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CustomProductsCell.self, forCellWithReuseIdentifier: "cellProducts")
        return cv
    }()
    
    fileprivate let spotlightsCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CustomCellSpotlights.self, forCellWithReuseIdentifier: "cellSpotlights")
        return cv
    }()
    
    fileprivate let imageUser: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "user.png")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    fileprivate let digioCashImage: UIImageView = {
        let digioImage = UIImageView()
        digioImage.translatesAutoresizingMaskIntoConstraints = false
        digioImage.contentMode = .scaleAspectFit
        return digioImage
    }()
    
    fileprivate let labelUserName: UILabel = {
        let labelName = UILabel()
        labelName.translatesAutoresizingMaskIntoConstraints = false
        labelName.font = UIFont(name: "Avenir-Next", size: 15.0)
        labelName.text = "Ola, Maria"
        return labelName
    }()
    
    fileprivate let labelProductsTitle: UILabel = {
        let labelTitleProducts = UILabel()
        labelTitleProducts.translatesAutoresizingMaskIntoConstraints = false
        labelTitleProducts.text = "Produtos"
        return labelTitleProducts
    }()
    
    fileprivate let labelDigioCashTitle: UILabel = {
        let labelTitleDigioCash = UILabel()
        labelTitleDigioCash.translatesAutoresizingMaskIntoConstraints = false
        return labelTitleDigioCash
    }()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getRepositories()
        self.setupView()
    }
    
    
    //MARK: - Layout
    func setupView() {
        
        view.addSubview(spotlightsCollectionView)
        spotlightsCollectionView.showsHorizontalScrollIndicator = false
        spotlightsCollectionView.backgroundColor = .white
        spotlightsCollectionView.delegate = self
        spotlightsCollectionView.dataSource = self
        spotlightsCollectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 140).isActive = true
        spotlightsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        spotlightsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        spotlightsCollectionView.heightAnchor.constraint(equalToConstant: view.frame.width/2).isActive = true
        
        view.addSubview(productsCollectionView)
        productsCollectionView.showsHorizontalScrollIndicator = false
        productsCollectionView.backgroundColor = .white
        productsCollectionView.delegate = self
        productsCollectionView.dataSource = self
        productsCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        productsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        productsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40).isActive = true
        productsCollectionView.heightAnchor.constraint(equalToConstant: view.frame.width/2).isActive = true
        
        
        view.addSubview(digioCashImage)
        digioCashImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        digioCashImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40).isActive = true
        digioCashImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        digioCashImage.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 60).isActive = true
        
        
        view.addSubview(imageUser)
        imageUser.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        imageUser.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        imageUser.widthAnchor.constraint(equalToConstant: 40).isActive = true
        imageUser.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        view.addSubview(labelUserName)
        labelUserName.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        labelUserName.leadingAnchor.constraint(equalTo: imageUser.leadingAnchor, constant: 50).isActive = true
        labelUserName.widthAnchor.constraint(equalToConstant: 100).isActive = true
        labelUserName.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        view.addSubview(labelProductsTitle)
        labelProductsTitle.bottomAnchor.constraint(equalTo: productsCollectionView.topAnchor, constant: 30).isActive = true
        labelProductsTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        labelProductsTitle.widthAnchor.constraint(equalToConstant: 100).isActive = true
        labelProductsTitle.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        view.addSubview(labelDigioCashTitle)
        labelDigioCashTitle.bottomAnchor.constraint(equalTo: digioCashImage.topAnchor, constant: 50).isActive = true
        labelDigioCashTitle.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
        labelDigioCashTitle.widthAnchor.constraint(equalToConstant: 100).isActive = true
        labelDigioCashTitle.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    
    
    
    //MARK: - API
    func getRepositories() {
        DigioAPI.getRepositories { (requestListModel, error) in
            guard let requestListModel = requestListModel else { return }
            
            // Spotlights
            guard let spotlights = requestListModel.spotlight else { return }
            self.didFetchSpotlightsRepositories(spotlightsRepositories: spotlights)
            
            // CashDigio
            guard let digioCash = requestListModel.cash else { return }
            self.labelDigioCashTitle.text = digioCash.title
            
            guard let url = URL(string: digioCash.bannerURL ?? "") else { return }
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    guard let image: UIImage = UIImage(data: data) else { return }
                    self.digioCashImage.image = image
                }
            }
            
            //Products
            guard let products = requestListModel.products else { return }
            self.didFetchProductsRepositories(productsRepositories: products)
            
        }
    }
    
    func didFetchSpotlightsRepositories(spotlightsRepositories: [SpotlightsModel]) {
        
        self.spotlightsModel.append(contentsOf: spotlightsRepositories)
        
        spotlightsCollectionView.reloadData()
        
    }
    
    func didFetchProductsRepositories(productsRepositories: [ProductsModel]) {
        
        self.productsModel.append(contentsOf: productsRepositories)
        
        productsCollectionView.reloadData()
        
    }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.spotlightsCollectionView {
            
            return CGSize(width: collectionView.frame.width + 2, height: collectionView.frame.width/2)
            
        } else {
            
            return CGSize(width: collectionView.frame.width/2.2, height: collectionView.frame.width/2.5)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.spotlightsCollectionView {
            
            return spotlightsModel.count
            
        } else {
            
            return productsModel.count
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.spotlightsCollectionView {
            let cellSpotlights = collectionView.dequeueReusableCell(withReuseIdentifier: "cellSpotlights", for: indexPath) as! CustomCellSpotlights
            if indexPath.item < self.spotlightsModel.count {
                cellSpotlights.data = self.spotlightsModel[indexPath.item]
            }
            
            return cellSpotlights
            
        } else {
            let cellProducts = collectionView.dequeueReusableCell(withReuseIdentifier: "cellProducts", for: indexPath) as! CustomProductsCell
            if indexPath.item < self.productsModel.count {
                cellProducts.data = self.productsModel[indexPath.item]
            }
            
            cellProducts.layer.cornerRadius = 12
            cellProducts.layer.borderWidth = 0.5
            cellProducts.layer.borderColor = UIColor.lightGray.cgColor
            
            return cellProducts
        }
        
    }
}

class CustomCellSpotlights: UICollectionViewCell {
    
    var data: SpotlightsModel? {
        didSet {
            guard let data = data else { return }
            if let imageProduct = data.bannerURL {
                let url = URL(string: imageProduct)
                self.bgSpotlights.kf.setImage(with: url)
            }
            
        }
    }
    
    fileprivate let bgSpotlights: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12
        
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        contentView.addSubview(bgSpotlights)
        
        bgSpotlights.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        bgSpotlights.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        bgSpotlights.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        bgSpotlights.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CustomProductsCell: UICollectionViewCell {
    
    var data: ProductsModel? {
        didSet {
            guard let data = data else { return }
            if let imageProduct = data.imageURL {
                let url = URL(string: imageProduct)
                self.bgProducts.kf.setImage(with: url)
            }
            
        }
    }
    
    fileprivate let bgProducts: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.clipsToBounds = true
        iv.layer.borderWidth = 0
        iv.layer.borderColor = UIColor.white.cgColor
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        contentView.addSubview(bgProducts)
        bgProducts.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16).isActive = true
        bgProducts.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16).isActive = true
        bgProducts.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16).isActive = true
        bgProducts.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
