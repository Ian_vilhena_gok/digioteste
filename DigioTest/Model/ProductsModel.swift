//
//  ProductsModel.swift
//  DigioTest
//
//  Created by Ian on 27/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import Foundation


public struct ProductsModel: Codable {
    
    public var name: String?
    public var imageURL: String?
    public var description: String?
    
    public init(name: String? = nil, imageURL: String? = nil, description: String? = nil) {
        
        self.name = name
        self.imageURL = imageURL
        self.description = description

    }
}
