//
//  DigioCashModel.swift
//  DigioTest
//
//  Created by Ian on 26/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import Foundation

public struct DigioCashModel: Codable {
    
    public var title: String?
    public var bannerURL: String?
    public var description: String?
    
    public init(title: String? = nil, bannerURL: String? = nil, description: String? = nil) {
        
        self.title = title
        self.bannerURL = bannerURL
        self.description = description

    }
}
