//
//  DigioAPI.swift
//  DigioTest
//
//  Created by Ian on 26/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import Foundation
import Alamofire

class DigioAPI: NSObject {
    
    private static var sharedInstance = DigioAPI()
    
    private var digioUrl = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/sandbox/products"
    
    static func getRepositories(completion: @escaping ((_ data: RequestListModel?,_ error: Error?) -> Void)) {
        
        AF.request(self.sharedInstance.digioUrl, method: .get).response {response in
        
            switch response.result {
                
            case .success(let data):
                guard let data = data else { return }
                
                guard let requestList = try? JSONDecoder().decode(RequestListModel.self, from: data) else {
                    return
                }
                
                completion(requestList, nil)
                
            case .failure(let error):
                print("Request failed with error: \(error)")
                completion(nil, error)
            }
            
        }
        
    }
    
}
