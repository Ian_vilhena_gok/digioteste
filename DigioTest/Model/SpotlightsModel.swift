//
//  SpotlightsModel.swift
//  DigioTest
//
//  Created by Ian on 26/08/20.
//  Copyright © 2020 Ian. All rights reserved.
//

import Foundation

public struct SpotlightsModel: Codable {
    
    public var name: String?
    public var bannerURL: String?
    public var description: String?
    
    public init(name: String? = nil, bannerURL: String? = nil, description: String? = nil) {
        
        self.name = name
        self.bannerURL = bannerURL
        self.description = description

    }
}
